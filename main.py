# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 12:17:17 2014

@author: dgevans
"""
import numpy as np
import pricing as AP
import pandas as pd
import pandas.io.data as web
import datetime

import matplotlib.pyplot as plt


##################
# Question 1     #
##################

#part a.
S=5
Pi = 0.0125*np.ones((5,5))
Pi += np.diag(0.95-0.0125*np.ones(5)) #construct lambda
lamb = np.array([1.025,1.015,1.,0.985,0.975]) #gamma
gamma = 2.
beta = 0.96

pf,Rf =  AP.riskFreePrice(Pi,lamb,gamma,beta)
print ("Risk Free Bond Price and return: ")
print (pf)
print (Rf)

#part b

nu,R =  AP.stockPrice(Pi,lamb,gamma,beta)
print ("Stock price and return: ")
print (nu)
print (R)


#part c
zeta = 1.
pc,Rc = AP.consolPrice(Pi,lamb,gamma,beta,zeta)
print ("Consol Bond Price and Return: ")
print (pc)
print (Rc)


#part d

p_s = 30.
w = AP.callOption(Pi,lamb,gamma,beta,zeta,p_s,20)
print ('Price of Call Option: ')
print (w)



##################
# Question 2     #
##################

#part a
start = datetime.datetime(1955, 1, 1)
end = datetime.datetime(2013, 1, 1)
df = web.DataReader("PCECC96", "fred", start, end)

#a
df = df.rename(columns={'PCECC96': 'C'})

#b
lagged = df.C.shift(1)
df['c_growth'] = (df['C']-lagged)/lagged

#c
df.drop(df.index[0],inplace=True)

#d
df['s'] = pd.qcut(df['c_growth'],10,[0,1,2,3,4,5,6,7,8,9])



#part b

#Task 1
lambda_data = np.zeros(10)
for i in range(10):
    lambda_data[i] = df.groupby('s').mean()['c_growth'][i]
Pi_data = np.full((10, 10), .1)


#part c

gamma_vec = np.linspace(0,20,50)
equityPremium_vec = np.zeros(len(gamma_vec))
sHist = df['s']
for i in range(len(gamma_vec)):
    equityPremium_vec[i] = AP.estimateEquityPremium(Pi_data, lambda_data, gamma_vec[i], 0.995, sHist)

plt.plot(gamma_vec, equityPremium_vec)
plt.show()
print("As far as I can see, you get a yearly equity premium of 6% (1.06) when gamma is > than 5.3ish. The higher your value of gamma(degree of risk aversion), the greater your equity Premium.")