# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 11:52:11 2014

@author: dgevans
"""
import numpy as np

def riskFreePrice(Pi,lamb,gamma,beta):
    '''
    Computes the price of a risk free bond
    
    Inputs
    -------------
    * Pi - Transition matrix (SxS array)
    * lamb - growth rate of consumption (S array)
    * gamma - coefficient of risk aversion (float)
    * beta - discount factor (float)
    
    Returns
    ---------
    * pf - Price of the risk free bond for each state of the world (length S array)
    * Rf - Return on the risk free bond for each state of the world
    '''
    S = len(lamb)
    pf = np.zeros(S)
    Rf = np.zeros(S)
    
    pf = (Pi.dot(lamb**(-gamma)))*beta
    Rf = 1/pf
    return pf, Rf
    
def stockPrice(Pi,lamb,gamma,beta):
    '''
    Computes the price to dividend ratio nu, and return on stock
    
    Inputs
    --------------
    * Pi - Transition matrix (SxS array)
    * lamb - growth rate of consumption (S array)
    * gamma - coefficient of risk aversion (float)
    * beta - discount factor (float)
    
    Returns
    ----------
    * nu - Price to dividend ratio for each state of the world
    * Rs - Return on the stock from state s to sprime
    '''
    S = len(lamb)
    c_t = lamb**(-gamma)
    dtilde = lamb*c_t
    ptilde_numerator = beta*Pi.dot(dtilde)
    nu = np.linalg.inv((np.eye(S)-beta*Pi*dtilde)).dot(ptilde_numerator)
    
    x = (S,S)
    Rs = np.zeros(x)
    for i in range(S):
        for j in range(S):
            Rs[i, j] = ((nu[j]+1)/nu[i])*lamb[j]
    return nu, Rs
    
def consolPrice(Pi,lamb,gamma,beta,zeta):
    '''
    Computes price of a consol bond with coupon zeta
    
    Inputs
    --------
    * Pi - Transition matrix (SxS array)
    * lamb - growth rate of consumption (S array)
    * gamma - coefficient of risk aversion (float)
    * beta - discount factor (float)
    * zeta - coupon (float)
    
    Returns
    ---------
    * pc - price of the consol in each state (length S array)
    * Rc - Return on the consol in each state (length S array)
    '''
    S = len(lamb)
    pc = np.zeros(S)
    d = np.ones(S)*zeta
    
    dtilde = beta*Pi.dot((lamb**(-gamma))*Pi.dot(d))
    pc = np.linalg.inv((np.eye(S)-(beta*Pi*(lamb**(-gamma))))).dot(dtilde)

    
    x = (S,S)
    Rc = np.zeros(x)
    
    for i in range(S):
        for j in range(S):
            Rc[i,j] = ((pc[j]+1)/pc[i])*d[j]
    return pc,Rc
        
    
def callOption(Pi,lamb,gamma,beta,zeta,pStrike,T, epsilon = 1e-8):
    '''
    Computes price of a call option on a consol bond with payoff zeta
    
    Inputs
    --------
    * Pi - Transition matrix (SxS array)
    * lamb - growth rate of consumption (S array)
    * gamma - coefficient of risk aversion (float)
    * zeta - coupon (float)
    * pStrike - strike price (float)
    * T - length of option (can be inf)
    * epsilon - tolerance for infinite horizon problem (float) (bonus question)
    
    Returns
    --------
    * w - price of option in each state of the world
    '''
    S = len(lamb)
    w = np.zeros([T+1, S])
    pc, Rc = consolPrice(Pi,lamb,gamma,beta,zeta)
    sell = pc-pStrike
    
    for t in range(T):
        keep = beta*Pi.dot(((w[t])*(lamb**(-gamma))))
        w[t+1][::] = (np.maximum(keep, sell))
    return w[T]

    
    
def estimateEquityPremium(Pi_data,lambda_data,gamma,beta,sHist):
    '''
    Estimates the equity premium
    
    Inputs:
    --------
    * Pi_data : estimated transition matrix
    * lambda_data: estimated consumption growth
    * gamma : degree of risk aversion
    * beta : discount rate 
    * sHist : sample path of states
    
    Returns:
    -----------
    * EP : estimate of equity premium
    '''
    S = len(lambda_data)
    T = len(sHist)
    lambda_data = (lambda_data*4)+1
    
    #Average return
    RF_returns = np.zeros(T-1)
    pf, Rf = riskFreePrice(Pi_data,(lambda_data),gamma,beta)
    avg_RF = 0
    for t in range(T-1):
        RF_returns[t] = Rf[sHist[t]]
        avg_RF += Rf[sHist[t]]
    avg_RF = avg_RF/T
    
    #Average equity return
    x = (T-1, T-1)
    E_returns = np.zeros(x)
    nu, Rs = stockPrice(Pi_data,(lambda_data),gamma,beta)
    Rs = Pi_data*Rs
    avg_E = 0
    for t in range(T-1):
        E_returns[t] = Rs[sHist[t], sHist[t+1]]
        avg_E += Rs[sHist[t], sHist[t+1]]
    avg_E = avg_E/T
    return (avg_RF-avg_E)
    
